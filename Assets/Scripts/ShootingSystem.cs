using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cinemachine;

public class ShootingSystem : MonoBehaviour
{

    InputSystem input;
    
    [SerializeField] ParticleSystem inkParticle;
    [SerializeField] Transform parentController;
    [SerializeField] Transform splatGunNozzle;
    [SerializeField] CinemachineFreeLook freeLookCamera;
    CinemachineImpulseSource impulseSource;
    private PlayerStatsManager playerStatsManager;
    private bool isShootSFXPlaying = false;

    void Start()
    {
        input = GetComponent<InputSystem>();
        impulseSource = freeLookCamera.GetComponent<CinemachineImpulseSource>();
        playerStatsManager = GetComponent<PlayerStatsManager>();
    }

    void Update()
    {
        Vector3 angle = parentController.localEulerAngles;
        //input.blockRotationPlayer = Input.GetMouseButton(0);
        bool pressing = input.sprint;

        if (pressing)
        {
            //VisualPolish();
            //input.RotateToCamera(transform);
        }

        if (pressing && playerStatsManager.staminaRatio > 0.01f)
        {
            if (!isShootSFXPlaying && !AudioManager.instance.sfxSource.isPlaying)
            {
                isShootSFXPlaying = true;
                AudioManager.instance.PlaySoundPersistent("peeing-more-bubbly", 10f);
                
            }
            inkParticle.Play();
        }
        else
        {
            isShootSFXPlaying = false;
            inkParticle.Stop();
            AudioManager.instance.sfxSource.Stop();
        }


        parentController.localEulerAngles= new Vector3(Mathf.LerpAngle(parentController.localEulerAngles.x, pressing ? RemapCamera(freeLookCamera.m_YAxis.Value, 0, 1, -25, 25) : 0, .3f), angle.y, angle.z);
    }

    void VisualPolish()
    {
        if (!DOTween.IsTweening(parentController))
        {
            parentController.DOComplete();
            Vector3 forward = -parentController.forward;
            Vector3 localPos = parentController.localPosition;
            parentController.DOLocalMove(localPos - new Vector3(0, 0, .2f), .03f)
                .OnComplete(() => parentController.DOLocalMove(localPos, .1f).SetEase(Ease.OutSine));

           impulseSource.GenerateImpulse();
        }

        if (!DOTween.IsTweening(splatGunNozzle))
        {
            splatGunNozzle.DOComplete();
            splatGunNozzle.DOPunchScale(new Vector3(0, 1, 1) / 1.5f, .15f, 10, 1);
        }
    }

    float RemapCamera(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
