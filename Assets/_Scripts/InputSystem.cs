using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputSystem : MonoBehaviour
{
    public Vector2 move;
    public Vector2 look;
    public PlayerInput _playerInput;
    public bool AnalogMovement => _playerInput.currentControlScheme != "KeyboardMouse";
    
    public bool jump;
    public bool sprint;
    public bool shoot;
    public bool cursorLocked = true;
    
    public bool cursorInputForLook = true;

    private void Start()
    {
        CinemachineInputProvider cineInput = transform.parent.GetComponentInChildren<CinemachineInputProvider>();
        cineInput.PlayerIndex = _playerInput.playerIndex;
    }

    
    

    public void OnMove(InputValue value)
    {
        MoveInput(value.Get<Vector2>());
    }

    public void OnLook(InputValue value)
    {
        if(cursorInputForLook)
        {
            LookInput(value.Get<Vector2>());
        }
    }

    public void OnJump(InputValue value)
    {
        JumpInput(value.isPressed);
    }

    public void OnShoot(InputValue value){
        ShootInput(value.isPressed);
    }

    public void OnSprint(InputValue value)
    {
        SprintInput(value.isPressed);
    }
    public void ShootInput(bool newShootState){
        shoot = newShootState;
    }
    public void MoveInput(Vector2 newMoveDirection)
    {
        move = newMoveDirection;
    } 

    public void LookInput(Vector2 newLookDirection)
    {
        look = newLookDirection;
    }

    public void JumpInput(bool newJumpState)
    {
        jump = newJumpState;
    }

    public void SprintInput(bool newSprintState)
    {
        sprint = newSprintState;
    }
    

    private void OnApplicationFocus(bool hasFocus)
    {
        SetCursorState(cursorLocked);
    }

    private void SetCursorState(bool newState)
    {
        Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
    }
}
